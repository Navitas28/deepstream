import pymongo
import json
from bson import json_util, ObjectId
import os

import datetime 
from datetime import date
from datetime import datetime

import os.path
from os import path


print("*****************DB Connection ************************************************")
#client = pymongo.MongoClient("mongodb://192.168.10.28:27017/bsnl_video_db")

db = client.bsnl_video_db
col = db.auth_details
col_second = db.Nxtgen_Data

def DataToDB(faceNames,timestamp,InOrOut,CamID, uid):
    timestamp = str(timestamp)
    dirname = os.path.dirname(__file__)
    
    for person in faceNames:
        try:
            AuthDetails = col.find({"pernr":person})
            AuthRaw = json_util.dumps(AuthDetails)
            AuthDict = json.loads(AuthRaw)
            fname = AuthDict[0]["fname"]
            lname = AuthDict[0]["lname"]
        except:
            fname = "Unknown"
            lname = "Unknown"

        try:
            myQuery = {"pernr":person,"date":datetime.now().strftime("%d%m%Y")}

        except:
            myQuery = {"pernr":0,"date":datetime.now().strftime("%d%m%Y")}
            



        findInRecDetail = col_second.find(myQuery)
        
        take_data = json_util.dumps(findInRecDetail)
        take_data_recog_dict = json.loads(take_data)

        #print("PREVIOUS ONE",timestamp)
        

        if(len(take_data_recog_dict) == 0):
            #if our dictionary is empty i.e. data is not available so we have to create new entry for this
            

            if(InOrOut == "in"):
                rawdata = {"fname":fname,"lname":lname,"pernr":person,"date":datetime.now().strftime("%d%m%Y"),"in_time":[{"timestamp":timestamp,"camID":CamID, "loc":uid}],"out_time":[],"is_in":True,"last_modified":timestamp,"url":""}
            else:
                rawdata = {"fname":fname,"lname":lname,"pernr":person,"date":datetime.now().strftime("%d%m%Y"),"out_time":[{"timestamp":timestamp,"camID":CamID, "loc":uid}],"in_time":[],"is_in":False,"last_modified":timestamp,"url":""}

            dump_in_db = col_second.insert_one(rawdata)
            
        else:
            
            #otherwise we have to update data of this date and employee
            existing_in_time = take_data_recog_dict[0]["in_time"]
            
            existing_out_time = take_data_recog_dict[0]["out_time"]
            
            existing_is_in = take_data_recog_dict[0]["is_in"]
            if(InOrOut == "in"):
                existing_in_time.append({"timestamp":timestamp,"camID":CamID, "loc":uid})
                new_is_in = True
            else:
                existing_out_time.append({"timestamp":timestamp,"camID":CamID, "loc":uid})
                new_is_in = False
            newvalues = {"$set": {"in_time":existing_in_time, "out_time":existing_out_time,"last_modified":timestamp,"is_in":new_is_in} }
            col_second.update_one(myQuery,newvalues)
           # print(newvalues)

    return 1



        
