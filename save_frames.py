import cv2
import numpy

streams = {
    'rtsp://10.192.13.30:554/live/0/main' : 'Cam1',
    'rtsp://10.192.13.32:554/live/0/main' : 'Cam2',
    'rtsp://10.192.13.33:554/live/0/main' : 'Cam3',
    'rtsp://10.192.13.34:554/live/0/main' : 'Cam4',
    'rtsp://10.192.13.35:554/live/0/main' : 'Cam5',
    'rtsp://10.192.13.36:554/live/0/main' : 'Cam6',

}

for i in streams.keys():
    cam = cv2.VideoCapture(i)
    print(f"Saving frame from {i}")

    ret, frame = cam.read()

    cv2.imwrite('./testing_frames/'+streams[i]+'.jpg', frame)

