import os
import sys
import time
from datetime import datetime
import logging
logging.basicConfig(filename='deepstream_restarts_nxtgen_annoy.log',filemode='a', level=logging.DEBUG)


while True:

    ds_seconds = datetime.now()
    with open('/home/nxtgen/.pm2/logs/nxtgen-prod-out.log', 'r') as f:
        while True:
            seconds = datetime.now()
            print("Seconds now "+str(seconds))
            print("Seconds from deepstream in " + str(ds_seconds))
            lines = f.readlines()
            last_4 = lines[:-1]
            #print(last_4)
            time.sleep(.3)
            try:
                #dt = datetime.strptime(time, '%Y-%m-%dT%H:%M:%S')
                ds_seconds = datetime.strptime(last_4[0].split(': R')[0], '%Y-%m-%dT%H:%M:%S')
                print(ds_seconds)
                print("read")
            except Exception as e:
                print(e)
                print("Could not read")

            tdiff = seconds - ds_seconds
            print(f"Time diff {tdiff.seconds} sec")
            if tdiff.seconds > 12:
                logging.debug(f"{datetime.now()} Restarted app")
                os.system('rm /home/nxtgen/.pm2/logs/nxtgen-prod-out.log')
                os.system('rm /home/nxtgen/.pm2/logs/nxtgen-prod-error.log')
                os.system('pm2 restart 0')
                time.sleep(20)
                # with open('/home/nxtgen/.pm2/logs')
                # logging.debug(last_4)
                logging.debug("***************")
                break

            # print("Seconds as string "+str(ds_seconds))


            # os.system('sleep 1 ')
            # print(last_4)
