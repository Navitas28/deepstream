from sklearn.preprocessing import Normalizer
from sklearn.preprocessing import LabelEncoder
from sklearn.neighbors import KNeighborsClassifier
from PIL import Image
import os
import joblib
import numpy as np
#import db_query_nxtgen_prod as db
from datetime import datetime
import logging
from http_api import send_post
import threading
import uuid
import pickle
from annoy import AnnoyIndex

time_now = datetime.now().strftime('%d-%m-%y')
print(time_now)
names_dict = {'0' : "Rcpt_Fourth_In", '1': "Rcpt_Fourth_Out", '2':'Rcpt_Third_In', '3':'Rcpt_Third_Out'}
f_name = f'/home/nxtgen/deepstream-fr/logs/deepstream_nxtgen_prod_{time_now}.log'

logging.basicConfig(filename=f_name,filemode='a', level=logging.INFO)

#names_dict = {'0' : "Rcpt_Fourth_In", '1': "Rcpt_Fourth_Out", '2':'Rcpt_Third_In', '3':'Rcpt_Third_Out'}
names_dict = {'0' : "Rcpt_Fourth_In", '1': "Rcpt_Fourth_Out", '2':'Rcpt_Third_In', '3':'Rcpt_Third_Out'}

in_out = ['in','out','in','out']
with open('./nxtgen_sep30_labels_dict.pkl', 'rb') as f:
	names = pickle.load(f)

t = AnnoyIndex(128,'dot')
t.load('nxtgen_oct01_facenet_dot.ann')


# def reset_list():
# 	if (datetime.now().strftime('%H:%M:%S')) == '00:00:00':

# 		in_list = []
# 		out_list = []

# 		logging.debug('Queues reset')
# 	return

KNN_CLASSIFIER_DICT = {"trained": False, "classifier": None}

def load_dataset(dataset_path):
	dataset_embeddings = np.load(dataset_path)
	faces_embeddings, labels = dataset_embeddings['arr_0'], dataset_embeddings['arr_1']
	faces_embeddings = faces_embeddings.reshape(-1,128)
	faces_embeddings = normalize_vectors(faces_embeddings)
	return faces_embeddings, labels

def normalize_vectors(vectors):
	# normalize input vectors
	normalizer = Normalizer(norm='l2')
	vectors = normalizer.transform(vectors)

	return vectors

def labels_encoder(labels):
	# label encode targets: one-hot encoding
	# this is needed by machine learning classifiers
	out_encoder = LabelEncoder()
	out_encoder.fit(labels)
	labels = out_encoder.transform(labels)
	return out_encoder, labels
	
	
def predict_using_classifier(faces_embeddings, labels, face_to_predict_embedding, stream_id, threshold=0.725):
	uid = "0"
	preds = t.get_nns_by_vector(face_to_predict_embedding[0,:],500,include_distances=True)

	print("Predicting")
	# print(time.time() - startT)
	pred = preds[0][0]
	dist = preds[1][0]

	# print(f"Predicted {names[pred]} with dist {dist}")
	# print(f" Initial Predicted = {out_encoder.inverse_transform(yhat_class)[0]} with {class_probability} from {stream_id}")
	predicted_name = names[pred]	
	class_probability = dist
	if class_probability >= threshold:
		
		uid = str(uuid.uuid1())
		print(f"Predicted = {predicted_name} with {class_probability} from {stream_id}")
		logging.info(str(datetime.now())+' predicted = '+str(predicted_name) + ' with prob '+ str(class_probability)+ ' from source : '+names_dict[str(stream_id)])
		try:
			#db.DataToDB([predicted_name.split('!')[0]], datetime.now(),in_out[stream_id], names_dict[str(stream_id)], "NxtGen_imgStore/"+uid+".jpg")
			th = threading.Thread(target=send_post, args=(str(predicted_name.split('!')[0]).lstrip('0'), str(datetime.now().strftime('%Y-%m-%d %H:%M:%S')), names_dict[str(stream_id)] ,in_out[stream_id]))
			th.start()
			th.join(timeout=2)
			# send_post(str([predicted_name.split('!')[0]]), str(datetime.now().strftime('%Y-%m-%d %H:%M:%S')), names_dict[str(stream_id)] ,in_out[stream_id])
		except:
			print("Failed to push to DB")
				
	else:
		predicted_name = 'Unknown'

	
	return predicted_name, class_probability, uid

